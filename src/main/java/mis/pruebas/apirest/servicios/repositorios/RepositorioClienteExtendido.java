package mis.pruebas.apirest.servicios.repositorios;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.modelos.CuentasCliente;

import java.util.List;

public interface RepositorioClienteExtendido {

    public void emparcharCliente(Cliente parche);

    public CuentasCliente obtenerCuentasCliente(String documento);
}
