package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cuenta;

import java.util.List;

public interface ServicioCuenta {

    public List<Cuenta> obtenerCuentas();

    public void insertarCuentaNueva(Cuenta cuenta);

    //REDA
    public Cuenta obtenerCuenta(String numero);

    //UPD
    public void guardarCuenta(Cuenta cuenta);
    public void emparcharCuenta(Cuenta parche);

    //DELETE
    public  void borrarCuenta(String numero);

    public Cuenta obtenerCuentaCliente(String documento, String numeroCuenta);

    public void eliminarCuentaCliente(String documento, String numeroCuenta);
}
